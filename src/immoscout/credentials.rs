use std::{fs, str, io::Read};

#[derive(Debug)]
pub struct Credentials {
    pub email: String,
    pub password: String,
}

pub fn read() -> Credentials {
    let mut file = match fs::File::open(".immoscout.pw") {
        Ok(file) => file,
        Err(msg) => panic!("could not read credentials file: {}", msg)
    };

    let mut contents = String::new();
    match file.read_to_string(&mut contents) {
        Ok(len) => len,
        Err(msg) => panic!("could not read from credential file: {}", msg)
    };

    let lines: Vec<&str> = contents.trim().split("\n").collect();
    
    Credentials {
        email: lines[0].to_string(),
        password: lines[1].to_string()
    }
}
