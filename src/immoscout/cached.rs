extern crate reqwest;

use std::{fs, str, io::Read, path::Path};
use sha2::{Sha512, Digest};

#[macro_export]
macro_rules! get {
    ($c: expr, $a: expr, $b: expr) => { cached::get($c, $a, $b) };
    ($c: expr, $a: expr) => { cached::get($c, $a, true) };
}

pub fn get(clnt: &reqwest::Client, url: &str, allow_cache: bool) -> Vec<u8> {
    let mut hasher = Sha512::new();
    hasher.input(url);
    let url_hash = format!("{:x}", hasher.result());
    let cache_dir = ".cache";
    let data_fn = format!("{}/{}.data", cache_dir, url_hash);
    let url_fn = format!("{}/{}.url", cache_dir, url_hash);
    let mut buffer = Vec::new();
    
    // check that cache file exists and then check that its a perfect match
    if allow_cache {
        match fs::read_to_string(&url_fn) {
            Ok(cached_url) => 
                if cached_url == url {
                    // found in cache!
                    println!("found {} in cache as {}", url, data_fn);
                    match fs::File::open(&data_fn) {
                        Ok(mut file) => {
                            file.read_to_end(&mut buffer).expect("could not read data file!");
                            return buffer;
                        },
                        _ => (),
                    };
                },
            _ => (),        
        };
        println!("url {} not yet in cache, retrieve!", url);
    } else {
        println!("caching for {} explicitely disabled!", url);
    };
    
    // not yet cached, retrieve and store to cache!
    let mut resp = clnt.get(url).send().unwrap();
    assert!(resp.status().is_success());

    // retrieve request body
    resp.read_to_end(&mut buffer).expect("could not read response data!");
    
    if allow_cache {
        if !Path::new(cache_dir).exists() {
            fs::create_dir(cache_dir).expect("could not create .cached subdir!");
        }
        fs::write(&data_fn, &buffer).expect("could not write cache data file!");
        fs::write(&url_fn, &url).expect("could not write cache url file!");
    }
    
    buffer
}

