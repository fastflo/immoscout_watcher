extern crate select;
extern crate reqwest;

use std::{fs, str, io::Read};
use std::collections::HashMap;

use select::document::Document;
use select::predicate::{And, Name, Attr};

#[macro_use]
mod cached;
mod credentials;

#[derive(Debug)]
struct Cookie {
    domain: String,
    path: String,
    name: String,
    
    value: String,
    expires: Option<String>,
    options: Vec<String> // Secure, HttpOnly
}

#[derive(Debug)]
struct PathCookies {
    path: String,
    names: HashMap<String, Cookie>,
}

#[derive(Debug)]
struct DomainCookies {
    domain: String,
    pathes: HashMap<String, PathCookies>,
}

#[derive(Debug)]
pub struct Immoscout {
    cred: credentials::Credentials,
    clnt: reqwest::Client,

    domain_cookies: HashMap<String, DomainCookies>,
}

macro_rules! get_mut_or_insert {
    ($hm: expr, $key: expr, $or_value: expr) => {
        match $hm.get_mut(&$key) {
            Some(value) => value,
            None => {
                $hm.insert($key.clone(), $or_value);
                $hm.get_mut(&$key).unwrap() // todo: how to avoid this lookup?
            }
        };
    };
}

fn to_string_option(opt_in: Option<&str>) -> Option<String> {
    match opt_in {
        Some(str_ref) => Some(str_ref.to_string()),
        None => None,
    }
}

impl Immoscout {
    pub fn new() -> Self {    
        let cred = credentials::read();
        println!("your credentials: {:?}", cred);
        
        let clnt = reqwest::Client::new();

        Self {
            cred, clnt,
            domain_cookies: HashMap::new(),
        }
    }

    fn consume_cookies(&mut self, resp: &reqwest::Response) {
        for (hdr, value) in resp.headers() {
            if hdr != "set-cookie" {
                continue;
            }
            let value = value.to_str().unwrap();
            // println!(" got cookie: {}", value);
            let pairs: Vec<_> = value.split(";").collect();
            let pairs: Vec<&str> = pairs.iter().map(|x| x.trim()).collect();
            
            let mut name = None;
            let mut value = None;
            let mut options = vec![];
                
            let mut kv_pairs = HashMap::new();
            for kv in pairs {
                let kv: Vec<&str> = kv.split("=").collect();
                if name == None {
                    name = Some(kv[0].to_string());
                    value = Some(kv[1].to_string());
                    continue;
                }
                if kv.len() == 1 {
                    options.push(kv[0].to_string());
                } else {
                    let value: &str = kv[1];
                    if value.len() >= 2 && value[..1] == "\""[..] && value[value.len() - 1..] == "\""[..] {
                        kv_pairs.insert(kv[0], Some(value[1..value.len() - 1].as_ref()));
                    } else {
                        kv_pairs.insert(kv[0], Some(value));
                    }
                }
            }
            let name = name.expect("set-cookie line without first name=value pair!");
            let value = value.unwrap();
            
            // println!("  cookie-pairs: {:?}", kv_pairs);
            if !kv_pairs.contains_key("Domain") {
                println!("ignore set-cookie without domain: {:?}", kv_pairs);
                continue;
            }
            if !kv_pairs.contains_key("Path") {
                println!("ignore set-cookie without path: {:?}", kv_pairs);
                continue;
            }
            let domain = kv_pairs["Domain"].unwrap().to_string();
            let path = kv_pairs["Path"].unwrap().to_string();
            
            let domain_cookies = get_mut_or_insert!(self.domain_cookies, domain, DomainCookies {
                domain: domain.clone(),
                pathes: HashMap::new()
            });            
            let path_cookies = get_mut_or_insert!(domain_cookies.pathes, path, PathCookies {
                path: path.clone(),
                names: HashMap::new()
            });
            
            let cookie = Cookie {
                domain, path,
                name: name.clone(), value,
                expires: to_string_option(kv_pairs["Expires"]),
                options
            };
            println!("set {:#?}", cookie);
            path_cookies.names.insert(name, cookie);
            
        }
        // println!("self.domain_cookies:\n{:#?}", self.domain_cookies);
    }

    fn inject_cookies(&self, req: reqwest::RequestBuilder, domain: &str, path: &str) -> reqwest::RequestBuilder {
        // todo:
        // get domain & path from url of req
        // insert cookie-headers for all found cookies into req        
        req
    }
    
    fn build_full_url(&self, domain: &str, path: &str) -> String {
        format!("https://{}{}", domain, path)
    }
    
    pub fn get(&mut self, domain: &str, path: &str) -> reqwest::RequestBuilder {
        let url = self.build_full_url(domain, path);
        let mut req = self.clnt.get(&url);
        self.inject_cookies(req, domain, path)
    }
    
    pub fn post(&mut self, domain: &str, path: &str) -> reqwest::RequestBuilder {
        let url = self.build_full_url(domain, path);
        let mut req = self.clnt.post(&url);
        self.inject_cookies(req, domain, path)
    }

    fn read_body(&mut self, resp: &mut reqwest::Response) -> String {
        assert!(resp.status().is_success());
        
        self.consume_cookies(&resp);
        
        let mut buffer = Vec::new();
        resp.read_to_end(&mut buffer).expect("could not read response data!");
        str::from_utf8(&buffer).unwrap().to_string()
    }
    
    pub fn login(&mut self) {
        let login_params = [
            ("appName", "is24main"),
            ("source", "meinkontodropdown-login"),
            ("sso_return", "https://www.immobilienscout24.de/sso/login.go")
        ];
        
        let mut resp = self.get("sso.immobilienscout24.de", "/sso/login")
            .query(&login_params)
            .send().unwrap();

        let body = self.read_body(&mut resp);
        let document = Document::from_read(body.as_bytes()).unwrap();
        
        let mut login_form = document.find(And(
            Name("form"),
            Attr("id", "loginForm")));
        let login_form = login_form.next().unwrap();

        let email_copy = self.cred.email.to_string();
        let mut params = vec![("username", email_copy.as_str())];
        for input in login_form.find(And(Name("input"), Attr("type", "hidden"))) {
            println!("  found hidden form input: {:?}", input);
            params.push(
                (input.attr("name").unwrap(), input.attr("value").unwrap()));
        }
        
        println!("form params: {:?}", params);
        // todo: have wrapper for posts, that also does caching but only if pragma: no-cache is not set
        let mut resp = self.post("sso.immobilienscout24.de", "/sso/login")
            .query(&login_params)
            .form(&params)
            .send().unwrap();
        let body = self.read_body(&mut resp);
        
        // println!("got pw headers:\n{:?}", resp.headers());
        
        fs::write("last.html", &body).expect("could not write last.html file!");
        println!("got pw body:\n{}", body);
    }    
}
