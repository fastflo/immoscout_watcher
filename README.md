to be able to login to immobilienscout24.de you need to create a file
named ".immoscout.pw" in your current working directory: first line is the
email you registered with https://www.immobilienscout24.de and 2nd line is
the password to use.

initial inspiration from: https://kadekillary.work/post/webscraping-rust/
rust book: https://doc.rust-lang.org/book
rust std api: https://doc.rust-lang.org/std/string/struct.String.html
